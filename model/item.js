const mongoose = require('mongoose');

//定义表的结构与字段规则
const itemSchema = new mongoose.Schema({
  itemName:{
    type: String,
    require: true
  },
  itemPrice:{
    type: Number,
    required: true
  },
  itemStore: {
    type: Number,
    default: 0
  },
  itemIntro:{
    type: String,
    default: ''
  },
  itemThumb: String,
  isSale:{
    type: Number,
    default: '0'  // 0 代表上架，1代表下架
  },
  content: String
});

// 创建表
const itemModel = mongoose.model('ys_items', itemSchema);

module.exports = itemModel;
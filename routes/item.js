const itemRouter = require('express').Router();
const itemModel = require('../model/item');

//请求商品列表
itemRouter.get('/itemlist', (req, res) => {
  itemModel.find().sort({ _id: -1 }).then(ret => {
    res.render('itemlist', {
      result: ret
    })
  }).catch(err => {
    res.render('itemlist', {
      result: []
    })
  })

})
//添加商品页
itemRouter.get('/additem', (req, res) => {
  res.render('additem', {})
})
//添加商品
itemRouter.post('/additem', (req, res) => {
  itemModel.insertMany(
    req.body
  ).then(ret => {
    res.send({
      code: 0,
      msg: '新增商品成功'
    })
  }).catch(err => {
    res.send({
      code: 0,
      msg: '新增商品成功'
    })
  })
})
//设置页
itemRouter.get('/setting', (req, res) => {
  res.render('setting')
})
module.exports = itemRouter;

const adminRouter = require('express').Router();

//处理路由请求
adminRouter.get('/admin',(req,res)=>{
  res.render('admin');
})
//进入界面仪表盘
adminRouter.get('/dashboard',(req,res)=>{
  res.render('dashboard');
})
module.exports = adminRouter;
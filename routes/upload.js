const uploadRouter = require('express').Router();
const upload = require('../utils/upload')

// 上传文件路由
uploadRouter.post('/upload', upload.single('file'), (req,res) => {
  console.log(req.file);
  res.send({
    code: 0,
    msg: '上传成功',
    data: {
      url: '/static/uploads/' + req.file.filename
    }
  })
})
module.exports = uploadRouter;
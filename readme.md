### 项目技术栈介绍
  + 本项目使用的数据库是mongo数据库
  + 页面渲染使用的是ejs的模板引擎
  + 本项目是基于node.js来开发的
### 项目文件结构
  + model文件夹 mongoose的配置目录，包括连接驱动和创建表的js
  + routes文件夹  接口文件
  + Utils文件夹 工具类函数存放的位置，上传的逻辑处理
  + views文件夹 页面文件
  + app.js 项目启动文件
### 项目运行
  + 打开mongo数据库
  + 运行项目 npm run dev
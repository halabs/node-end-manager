const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
//连接数据库
require('./model/connect');
//导入路由
const adminRouter = require('./routes/admin');
const itemRouter = require('./routes/item');
const uploadRouter = require('./routes/upload');
const app = express();
//设置post请求参数
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//静态资源访问路径
app.use('/static', express.static(path.join(__dirname, 'public')))
//views的访问路径
app.set('views', path.join(__dirname, 'views'))
//使用模板引擎
app.set('view engine', 'ejs')

//使用路由
app.use(adminRouter);
app.use(itemRouter);
app.use(uploadRouter);
app.listen(3000, function () {
  console.log('启动了3000端口');
})